<?php

class Member {
    private $id;
    private $firstname;
    private $lastname;
    private $email;
    private $telephone;
    private $date_naissance;
    private $portfolio;
    private $password;
    private $roles;

    public function __construct($id = null, $firstname = null, $lastname = null, $email = null, $telephone = null, $date_naissance = null, $portfolio = null, $password = null, $roles = null)
    {
        $this->id = $id;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->email = $email;
        $this->telephone = $telephone;
        $this->date_naissance = $date_naissance;
        $this->portfolio = $portfolio;
        $this->password = $password ?? ''; // set default value to empty string if null
        $this->roles = $roles;
    }

    // Getter and setter methods for each property
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFirstname() {
        return $this->firstname ?? '';
    }

    /**
     * @param string $firstname
     */
    public function setFirstname(string $firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param string|null $lastname
     */
    public function setLastname(?string $lastname): void
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    /**
     * @param string|null $telephone
     */
    public function setTelephone(?string $telephone): void
    {
        $this->telephone = $telephone;
    }

    /**
     * @return string
     */
    public function getDateNaissance(): ?string
    {
        return $this->date_naissance;
    }

    /**
     * @param string|null $date_naissance
     */
    public function setDateNaissance(?string $date_naissance): void
    {
        $this->date_naissance = $date_naissance;
    }

    /**
     * @return string
     */
    public function getPortfolio(): ?string
    {
        return $this->portfolio;
    }

    /**
     * @param string|null $portfolio
     */
    public function setPortfolio(?string $portfolio): void
    {
        $this->portfolio = $portfolio;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password ?? ''; // return empty string if null
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getRoles(): ?string
    {
        return $this->roles;
    }

    /**
     * @param string|null $roles
     */
    public function setRoles(?string $roles): void
    {
        $this->roles = $roles;
    }
}