<?php
require_once './dal/bd.hackathon.inc.php';
require_once './dal/bd.inc.php';
require_once './model/Hackathon.php';
require_once './model/Member.php';

class Assistance {
    private $id;
    private $idhackathon;
    private $idmember;
    private $titre;
    private $description;

    public function __construct($id, $idhackathon, $idmember, $titre, $description) {
        $this->id = $id;
        $this->idhackathon = $idhackathon;
        $this->idmember = $idmember;
        $this->titre = $titre;
        $this->description = $description;
    }

    public function getId() {
        return $this->id;
    }

    public function getIdHackathon() {
        return $this->idhackathon;
    }

    public function getIdMember() {
        return $this->idmember;
    }

    public function getTitre() {
        return $this->titre;
    }

    public function setTitre($titre) {
        $this->titre = $titre;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public static function create($idhackathon, $idmember, $titre, $description) {
        $conn = connexionPDO();
        $query = 'INSERT INTO assistance (idhackathon, idmember, titre, description) VALUES (:idhackathon, :idmember, :titre, :description)';
        $stmt = $conn->prepare($query);
        $stmt->bindParam(':idhackathon', $idhackathon, PDO::PARAM_INT);
        $stmt->bindParam(':idmember', $idmember, PDO::PARAM_INT);
        $stmt->bindParam(':titre', $titre, PDO::PARAM_STR);
        $stmt->bindParam(':description', $description, PDO::PARAM_STR);
        $stmt->execute();
        $id = $conn->lastInsertId();
        $conn = null;
        return new Assistance($id, $idhackathon, $idmember, $titre, $description);
    }

    public static function getByHackathon($idhackathon) {
        $conn = connexionPDO();
        $query = 'SELECT * FROM assistance WHERE idhackathon = :idhackathon';
        $stmt = $conn->prepare($query);
        $stmt->bindParam(':idhackathon', $idhackathon, PDO::PARAM_INT);
        $stmt->execute();
        $assistanceList = array();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $assistance = new Assistance($row['id'], $row['idhackathon'], $row['idmember'], $row['titre'], $row['description']);
            $assistanceList[] = $assistance;
        }
        $conn = null;
        return $assistanceList;
    }

    public function createAssistance() {
        try {
            $conn = connexionPDO();
            $stmt = $conn->prepare("INSERT INTO assistance(idhackathon, idmember, titre, description) VALUES (:idhackathon, :idmember, :titre, :description)");
            $stmt->bindParam(':idhackathon', $this->idhackathon, PDO::PARAM_INT);
            $stmt->bindParam(':idmember', $this->idmember, PDO::PARAM_INT);
            $stmt->bindParam(':titre', $this->titre, PDO::PARAM_STR);
            $stmt->bindParam(':description', $this->description, PDO::PARAM_STR);
            $stmt->execute();
            return true;
        } catch (PDOException $e) {
        echo $e->getMessage();
        return false;
    }
}

}
?>
