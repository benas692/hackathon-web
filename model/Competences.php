<?php
require_once './dal/bd.inc.php';
require_once './model/Hackathon.php';
require_once './model/Member.php';

class Competences {
    private $id;
    private $idHackathon;
    private $idMember;
    private $titre;
    private $description;

    public function __construct($id, $idHackathon, $idMember, $titre, $description) {
        $this->id = $id;
        $this->idHackathon = $idHackathon;
        $this->idMember = $idMember;
        $this->titre = $titre;
        $this->description = $description;
    }

    public function getId() {
        return $this->id;
    }

    public function getIdHackathon() {
        return $this->idHackathon;
    }

    public function setIdHackathon($idHackathon) {
        $this->idHackathon = $idHackathon;
    }

    public function getIdMember() {
        return $this->idMember;
    }

    public function setIdMember($idMember) {
        $this->idMember = $idMember;
    }

    public function getTitre() {
        return $this->titre;
    }

    public function setTitre($titre) {
        $this->titre = $titre;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public static function create($idHackathon, $idMember, $titre, $description) {
        $db = connexionPDO();
        $query = 'INSERT INTO competences (hackathonid, memberid, titre, description) VALUES (:hackathonid, :memberid, :titre, :description)';
        $stmt = $db->prepare($query);
        $stmt->bindParam(':hackathonid', $idHackathon, PDO::PARAM_INT);
        $stmt->bindParam(':memberid', $idMember, PDO::PARAM_INT);
        $stmt->bindParam(':titre', $titre, PDO::PARAM_STR);
        $stmt->bindParam(':description', $description, PDO::PARAM_STR);
        $stmt->execute();
        $id = $db->lastInsertId();
        $db = null;
        return new Competences($id, $idHackathon, $idMember, $titre, $description);
    }

    public static function getByHackathon($idHackathon) {
        $db = connexionPDO();
        $query = 'SELECT * FROM competences WHERE hackathonid = :hackathonid';
        $stmt = $db->prepare($query);
        $stmt->bindParam(':hackathonid', $idHackathon, PDO::PARAM_INT);
        $stmt->execute();
        $competencesList = array();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $competence = new Competences($row['id'], $row['hackathonid'], $row['memberid'], $row['titre'], $row['description']);
            $competencesList[] = $competence;
        }
        $db = null;
        return $competencesList;
    }

    public function createCompetences()
    {
        try {
            $conn = connexionPDO();
            $stmt = $conn->prepare("INSERT INTO competences(idhackathon, idmember, titre, description) VALUES (:idhackathon, :idmember, :titre, :description)");
            $stmt->bindParam(':idhackathon', $this->idhackathon, PDO::PARAM_INT);
            $stmt->bindParam(':idmember', $this->idmember, PDO::PARAM_INT);
            $stmt->bindParam(':titre', $this->titre, PDO::PARAM_STR);
            $stmt->bindParam(':description', $this->description, PDO::PARAM_STR);
            $stmt->execute();
            return true;
        } catch (PDOException $e) {
            echo $e->getMessage();
            return false;
        }
    }
}
