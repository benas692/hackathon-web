<?php

class Hackathon
{
    private int $id;
    private string $name;
    private string $topic;
    private string $description;
    private ?string $date_debut;
    private ?string $date_fin;
    private ?string $date_fin_inscription;
    private ?int $nombre_participants;
    private ?int $inscriptiontypeid;
    private ?int $lieuid;
    private ?int $phaseid;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getTopic(): string
    {
        return $this->topic;
    }

    public function setTopic(string $topic): void
    {
        $this->topic = $topic;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getDateDebut(): ?string
    {
        return $this->date_debut;
    }

    public function setDateDebut(?string $date_debut): void
    {
        $this->date_debut = $date_debut;
    }

    public function getDateFin(): ?string
    {
        return $this->date_fin;
    }

    public function setDateFin(?string $date_fin): void
    {
        $this->date_fin = $date_fin;
    }

    public function getDateFinInscription(): ?string
    {
        return $this->date_fin_inscription;
    }

    public function setDateFinInscription(?string $date_fin_inscription): void
    {
        $this->date_fin_inscription = $date_fin_inscription;
    }

    public function getNombreParticipants(): ?int
    {
        return $this->nombre_participants;
    }

    public function setNombreParticipants(?int $nombre_participants): void
    {
        $this->nombre_participants = $nombre_participants;
    }

    public function getInscriptionTypeId(): ?int
    {
        return $this->inscriptiontypeid;
    }

    public function setInscriptionTypeId(?int $inscriptiontypeid): void
    {
        $this->inscriptiontypeid = $inscriptiontypeid;
    }

    public function getLieuId(): ?int
    {
        return $this->lieuid;
    }

    public function setLieuId(?int $lieuid): void
    {
        $this->lieuid = $lieuid;
    }

    public function getPhaseId(): ?int
    {
        return $this->phaseid;
    }

    public function setPhaseId(?int $phaseid): void
    {
        $this->phaseid = $phaseid;
    }

    public function getDate() {
        return $this->date;
    }
}