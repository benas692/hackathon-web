<?php

class Participation {
    private $id;
    private $hackathonid;
    private $memberid;
    private $roleid;
    private $equipeid;

    public function __construct($id, $hackathonid, $memberid, $roleid, $equipeid) {
        $this->id = $id;
        $this->hackathonid = $hackathonid;
        $this->memberid = $memberid;
        $this->roleid = $roleid;
        $this->equipeid = $equipeid;
    }

    public function getId() {
        return $this->id;
    }

    public function getHackathonId() {
        return $this->hackathonid;
    }

    public function getMemberId() {
        return $this->memberid;
    }

    public function getRoleId() {
        return $this->roleid;
    }

    public function getEquipeId() {
        return $this->equipeid;
    }

    public function setHackathonId($hackathonid) {
        $this->hackathonid = $hackathonid;
    }

    public function setMemberId($memberid) {
        $this->memberid = $memberid;
    }

    public function setRoleId($roleid) {
        $this->roleid = $roleid;
    }

    public function setEquipeId($equipeid) {
        $this->equipeid = $equipeid;
    }

    public function getMembersForHackathon($hackathonId) {
        $members = array();
        // Récupérer les participations pour ce hackathon
        $participations = $this->getParticipationsForHackathon($hackathonId);
        // Récupérer les membres correspondants aux participations
        foreach ($participations as $participation) {
            $member = new Member($participation->getMemberId());
            if (!empty($member->getFirstname())) {
                $members[] = $member;
            }
        }
        return $members;
    }

    public function getParticipationsForHackathon($hackathonId) {
        // Connexion à la base de données
        $dsn = 'pgsql:host=195.221.64.108;port=5432;dbname=hackathon;user=abdousalam;password=abdousalam';
        $db = new PDO($dsn);
        // Préparation de la requête
        $query = "SELECT * FROM participation WHERE hackathonid = :hackathonId";

        // Exécution de la requête avec un paramètre
        $stmt = $db->prepare($query);
        $stmt->bindValue(':hackathonId', $hackathonId, PDO::PARAM_INT);
        $stmt->execute();

        // Récupération des résultats sous forme d'objets Participation
        $participations = array();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $participation = new Participation(
                $row['id'],
                $row['hackathonid'],
                $row['memberid'],
                $row['roleid'],
                $row['equipeid']
            );
            $participations[] = $participation;
        }

        return $participations;
    }

    public function getRoleIdByUserId($user_id) {
        // Connexion à la base de données
        $dsn = 'pgsql:host=195.221.64.108;port=5432;dbname=hackathon;user=abdousalam;password=abdousalam';
        $db = new PDO($dsn);

        // Préparation de la requête
        $query = "SELECT roleid FROM participation WHERE memberid = :userId";

        // Exécution de la requête avec un paramètre
        $stmt = $db->prepare($query);
        $stmt->bindValue(':userId', $user_id, PDO::PARAM_INT);
        $stmt->execute();

        // Récupération du résultat
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        // Vérification si le résultat est non vide, et retour de l'ID du rôle
        if (!empty($result)) {
            return $result['roleid'];
        } else {
            return null;
        }
    }
}