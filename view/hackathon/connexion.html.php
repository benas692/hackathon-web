<?php
session_start();
require_once './dal/bd.hackathon.inc.php';
require_once './model/Member.php';
include "$root/view/header.html.php";

// Vérification des données envoyées en POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $email = $_POST['email'];
    $password = $_POST['password'];

    // Initialisation de la connexion à la base de données
    $pdo = getPdo();

    // Récupération du membre correspondant à l'adresse email fournie
    $member = getMemberByEmail($pdo, $email);
    // Vérification du mot de passe
    if ($member && verifyPassword($email, $password, $member->getPassword())) {
        // Authentification réussie
        $_SESSION["email"] = $member->getEmail();
        // Stocker les autres informations de la ligne dans la session
        $_SESSION["id"] = $member->getId();
        $_SESSION["firstname"] = $member->getFirstname();
        $_SESSION["lastname"] = $member->getLastname();
        $_SESSION["telephone"] = $member->getTelephone();
        $_SESSION["date_naissance"] = $member->getDateNaissance();
        $_SESSION["portfolio"] = $member->getPortfolio();

        header('Location: index.php?objet=site&action=presentation');
        exit();
    } else {
        // Authentification échouée
        $error = 'Mauvaise adresse email ou mot de passe';
    }
}

function verifyPassword(string $email, string $password, string $hash): bool {
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("SELECT password FROM member WHERE email = ?");
        $req->execute([$email]);
        $hash = $req->fetchColumn();

        return hash_equals($hash, $password);

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
}

function getMemberByEmail(PDO $pdo, string $email): ?Member
{
    $stmt = $pdo->prepare('SELECT * FROM member WHERE email = ?');
    $stmt->execute([$email]);
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($row) {
        $member = new Member();
        $member->setId($row['id']);
        $member->setFirstname($row['firstname']);
        $member->setLastname($row['lastname']);
        $member->setEmail($row['email']);
        $member->setTelephone($row['telephone']);
        $member->setDateNaissance($row['date_naissance']);
        $member->setPortfolio($row['portfolio']);
        $member->setPassword($row['password']);
        $member->setRoles($row['roles']);
        if ($member->getEmail() == $email) {
            return $member;
        }
    }
    return null;
}

// Affichage du contenu de la table "member"
$pdo = getPdo();
$stmt = $pdo->query('SELECT * FROM member');
$members = $stmt->fetchAll(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html>
<head>
    <title>Connexion</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }

        h1 {
            text-align: center;
        }

        form {
            max-width: 400px;
            margin: 0 auto;
        }

        label {
            display: inline-block;
            width: 100px;
            text-align: right;
            margin-right: 10px;
        }

        input[type="email"], input[type="password"], input[type="submit"] {
            display: block;
            margin-bottom: 10px;
            width: 100%;
            padding: 5px;
            font-size: 16px;
            border-radius: 5px;
            border: none;
        }

        input[type="submit"] {
            background-color: #1c87c9;
            color: #fff;
            cursor: pointer;
            transition: background-color 0.2s ease-in-out;
        }

        input[type="submit"]:hover {
            background-color: #156299;
        }

        .error {
            color: #f00;
            margin-bottom: 10px;
        }

        .success {
            color: #0a0;
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
<h1>Connexion</h1>
<?php if (isset($error)): ?>
    <div class="error"><?= $error ?></div>
<?php endif; ?>
<?php if (isset($_GET['message']) && $_GET['message'] == 'connexion_reussie'): ?>
    <div class="success">Connexion réussie !</div>
<?php endif; ?>
<form method="POST">
    <label for="email">Adresse email :</label>
    <input type="email" name="email" id="email" required><br><br>
    <label for="password">Mot de passe :</label>
    <input type="password" name="password" id="password" required><br><br>

    <input type="submit" value="Se connecter">
</form>
</body>
</html>
