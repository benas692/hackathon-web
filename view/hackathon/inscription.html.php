<?php
require_once './dal/bd.hackathon.inc.php';
require_once './model/Member.php';
include "$root/view/header.html.php";

// Vérifier si le formulaire a été soumis
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // Récupérer les données du formulaire
    $firstname = $_POST["firstname"];
    $lastname = $_POST["lastname"];
    $email = $_POST["email"];
    $telephone = $_POST["telephone"];
    $date_naissance = $_POST["date_naissance"];
    $portfolio = $_POST["portfolio"];
    $password = $_POST["password"];
    $roles = '["ROLE_USER"]'; // par défaut, le rôle est "user"

    // Vérifier si l'utilisateur existe déjà dans la base de données
    $conn = pg_connect("host=195.221.64.108 dbname=hackathon user=abdousalam password=abdousalam");
    $result = pg_query_params($conn, "SELECT * FROM member WHERE email = $1", array($email));
    if (pg_num_rows($result) > 0) {
        echo "Cet utilisateur existe déjà dans la base de données.";
        exit();
    }

    // Insérer l'utilisateur dans la base de données
    $result = pg_query_params($conn, "INSERT INTO member (firstname, lastname, email, telephone, date_naissance, portfolio, password, roles) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)", array($firstname, $lastname, $email, $telephone, $date_naissance, $portfolio, $password, $roles));
    if (!$result) {
        echo "Erreur lors de l'insertion de l'utilisateur dans la base de données.";
        exit();
    }

    // Rediriger l'utilisateur vers la page de connexion
    header("Location: index.php?object=hackathon&action=connexion");
    exit();
}

// Afficher le formulaire d'inscription
?>
<!DOCTYPE html>
<html>
<head>
    <title>Inscription</title>
    <style>
        body {
            background-color: #f5f5f5;
            font-family: Arial, sans-serif;
        }

        h1 {
            font-size: 2.5rem;
            text-align: center;
            margin-top: 30px;
            margin-bottom: 30px;
        }

        form {
            max-width: 500px;
            margin: 0 auto;
            background-color: #fff;
            padding: 30px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px #888888;
        }

        label {
            font-size: 1.1rem;
            display: block;
            margin-bottom: 5px;
        }

        input[type="text"],
        input[type="email"],
        input[type="tel"],
        input[type="url"],
        input[type="password"],
        input[type="date"] {
            font-size: 1rem;
            padding: 10px;
            border-radius: 5px;
            border: none;
            width: 100%;
            margin-bottom: 15px;
        }

        input[type="submit"] {
            background-color: #4CAF50;
            color: white;
            font-size: 1.2rem;
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }

        input[type="submit"]:hover {
            background-color: #3e8e41;
        }

        input[type="submit"]:active {
            background-color: #2c662d;
        }
    </style>
</head>
<body>
<h1>Inscription</h1>
<form method="post">
    <label for="firstname">Prénom :</label>
    <input type="text" name="firstname" id="firstname" required>

    <label for="lastname">Nom :</label>
    <input type="text" name="lastname" id="lastname" required>

    <label for="email">Email :</label>
    <input type="email" name="email" id="email"  required>

    <label for="telephone">Téléphone :</label>
    <input type="tel" name="telephone" id="telephone" required>

    <label for="date_naissance">Date de naissance :</label>
    <input type="date" name="date_naissance" id="date_naissance" required>

    <label for="portfolio">Portfolio :</label>
    <input type="url" name="portfolio" id="portfolio" required>

    <label for="password">Mot de passe :</label>
    <input type="password" name="password" id="password" required>

    <input type="submit" value="S'inscrire">
</form>
</body>
</html>