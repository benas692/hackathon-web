<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once './dal/bd.hackathon.inc.php';
require_once './model/Member.php';
include "$root/view/header.html.php";

// Vérifier si l'utilisateur est connecté
if (!isset($_SESSION["id"]) && !isset($_SESSION["email"])) {
    header("Location: index.php?object=site&action=presentation");
    exit();
}

// Récupérer l'identifiant ou l'email de l'utilisateur connecté
$id = $_SESSION['id'];
$email = $_SESSION['email'];

// Récupérer les informations de l'utilisateur depuis la base de données
$conn = pg_connect("host=195.221.64.108 dbname=hackathon user=abdousalam password=abdousalam");
$result = pg_query_params($conn, "SELECT * FROM member WHERE email = $1", array($email));
if (pg_num_rows($result) > 0) {
    $row = pg_fetch_assoc($result);
    $firstname = $row['firstname'];
    $lastname = $row['lastname'];
    $telephone = $row['telephone'];
    $date_naissance = $row['date_naissance'];
    $portfolio = $row['portfolio'];
}

// Vérifier si le formulaire a été soumis
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // Récupérer les données du formulaire
    $firstname = $_POST["firstname"];
    $lastname = $_POST["lastname"];
    $telephone = $_POST["telephone"];
    $date_naissance = $_POST["date_naissance"];
    $portfolio = $_POST["portfolio"];
    $password = $_POST["password"];
    $password_confirm = $_POST["password_confirm"];

    // Vérifier si les mots de passe correspondent
    if ($password !== $password_confirm) {
        echo "Les mots de passe ne correspondent pas.";
        exit();
    }

    // Mettre à jour les informations de l'utilisateur dans la base de données
    $result = pg_query_params($conn, "UPDATE member SET firstname = $1, lastname = $2, telephone = $3, date_naissance = $4, portfolio = $5, password = $6 WHERE email = $7", array($firstname, $lastname, $telephone, $date_naissance, $portfolio, $password, $email));
    if (!$result) {
        echo "Erreur lors de la mise à jour des informations de l'utilisateur dans la base de données.";
        exit();
    }

    // Rediriger l'utilisateur vers la page de profil
    header("Location: index.php?object=site&action=presentation");
    exit();
}

// Afficher le formulaire de modification d'informations de compte
?>
    <!DOCTYPE html>
    <html>
    <head>
        <title>Modifier mes informations</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
                background-color: #f7f7f7;
            }

            h1 {
                font-size: 2em;
                margin-bottom: 20px;
            }

            form {
                background-color: #fff;
                padding: 20px;
                border-radius: 5px;
                box-shadow: 0px 2px 5px rgba(0,0,0,0.2);
                max-width: 500px;
                margin: 0 auto;
            }

            label {
                display: block;
                margin-bottom: 5px;
            }

            input[type="text"],
            input[type="email"],
            input[type="tel"],
            input[type="date"],
            input[type="url"],
            input[type="password"] {
                padding: 10px;
                border-radius: 5px;
                border: none;
                width: 100%;
                margin-bottom: 10px;
                box-sizing: border-box;
            }

            input[type="submit"] {
                background-color: #4CAF50;
                color: #fff;
                padding: 10px 20px;
                border: none;
                border-radius: 5px;
                cursor: pointer;
                font-size: 1em;
                transition: background-color 0.2s ease-in-out;
            }

            input[type="submit"]:hover {
                background-color: #3e8e41;
            }
        </style>
    </head>
    <body>
<h1>Modifier mes informations</h1>
<form method="POST">
    <label for="firstname">Prénom :</label>
    <input type="text" name="firstname" id="firstname" value="<?= $firstname ?>" required><br>

    <label for="lastname">Nom :</label>
    <input type="text" name="lastname" id="lastname" value="<?= $lastname ?>" required><br>

    <label for="telephone">Téléphone :</label>
    <input type="tel" name="telephone" id="telephone" value="<?= $telephone ?>" required><br>

    <label for="date_naissance">Date de naissance :</label>
    <input type="date" name="date_naissance" id="date_naissance" value="<?= $date_naissance ?>" required><br>

    <label for="portfolio">Portfolio :</label>
    <input type="url" name="portfolio" id="portfolio" value="<?= $portfolio ?>" required><br>

    <label for="password">Mot de passe :</label>
    <input type="password" name="password" id="password" required><br>

    <label for="password_confirm">Confirmer le mot de passe :</label>
    <input type="password" name="password_confirm" id="password_confirm" required><br>

    <input type="submit" value="Enregistrer">
</form>
</body>
</html>
<?php include "$root/view/footer.html.php"; ?>