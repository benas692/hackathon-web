<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once './model/Member.php';
require_once './model/Participation.php';
require_once './dal/bd.hackathon.inc.php';
require_once './model/Assistance.php';
include "$root/view/header.html.php";

// Vérifier que l'utilisateur est connecté
if (isset($_SESSION['id'])) {
    $user_id = $_SESSION['id'];
}

?>

    <h1><?php print($hackathon->getName()) ?></h1>
    <table>
        <tr>
            <td><strong>Topic :</strong></td>
            <td><?php echo $hackathon->getTopic(); ?></td>
        </tr>
        <tr>
            <td><strong>Description :</strong></td>
            <td><?php echo $hackathon->getDescription(); ?></td>
        </tr>
    </table>

    <h2>Liste des participants :</h2>
<?php
// Connexion à la base de données
$dsn = 'pgsql:host=195.221.64.108;port=5432;dbname=hackathon;user=abdousalam;password=abdousalam';
$db = new PDO($dsn);
// Préparation de la requête avec jointures
$query = "SELECT m.firstname, m.lastname
FROM hackathon h
INNER JOIN participation p ON h.id = p.hackathonid
INNER JOIN member m ON p.memberid = m.id
WHERE h.id = :hackathonId";

// Exécution de la requête avec un paramètre
$stmt = $db->prepare($query);
$stmt->bindValue(':hackathonId', $hackathon->getId(), PDO::PARAM_INT);
$stmt->execute();

// Récupération des résultats sous forme d'objets Member
$members = array();
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $member = new Member(null, $row['firstname'], $row['lastname']);
    $members[] = $member;
}

if (!empty($members)) {
    echo '<ul>';
    foreach ($members as $member) {
        if (!empty($member->getFirstname())) {
            echo '<li>' . $member->getFirstname() . ' ' . $member->getLastname() . '</li>';
        }
    }
    echo '</ul>';
} else {
    echo '<p>Aucun participant pour le moment.</p>';
}
?>



<?php
// Si l'utilisateur est connecté, afficher le bouton pour une nouvelle inscription avec ses informations
if (isset($user_id)) {
    $inscription_link = "/?object=hackathon&action=details&id=" . $hackathon->getId() . "&memberid=" . $user_id . "&roleid=4&equipeid=null";
    print('<form method="POST"><input type="submit" name="inscription_submit" value="Nouvelle inscription"></form>');

    if (isset($_POST['inscription_submit'])) {
        $equipe_id = null;
        add_participation($hackathon->getId(), $user_id, 4, $equipe_id);
        redirect_to_hackathon_details($hackathon->getId());
    }

    // Afficher le bouton d'annulation d'inscription
    $participation = get_participation($hackathon->getId(), $user_id);
    if ($participation !== null) {
        print('<form method="POST"><input type="submit" name="cancel_inscription_submit" value="Annuler mon inscription"></form>');
        if (isset($_POST['cancel_inscription_submit'])) {
            delete_participation($participation->getId());
            redirect_to_hackathon_details($hackathon->getId());
        }
    }
}

function add_participation($hackathon_id, $member_id, $role_id, $equipe_id) {
    // Connexion à la base de données
    $connection = pg_connect("host=195.221.64.108 dbname=hackathon user=abdousalam password=abdousalam");
    $query = "INSERT INTO public.participation(hackathonid, memberid, roleid, equipeid) VALUES ('$hackathon_id', '$member_id', '$role_id', ";
    if ($equipe_id === null) {
        $query .= "null";
    } else {
        $query .= "'$equipe_id'";
    }
    $query .= ")";
    $result = pg_query($connection, $query);
}

function delete_participation($participation_id) {
    // Connexion à la base de données
    $connection = pg_connect("host=195.221.64.108 dbname=hackathon user=abdousalam password=abdousalam");
    $query = "DELETE FROM public.participation WHERE id='$participation_id'";
    $result = pg_query($connection, $query);
}

function redirect_to_hackathon_details($hackathon_id) {
    // Rediriger l'utilisateur vers la page de détails du hackathon
    header("Location: /?object=hackathon&action=details&id=" . $hackathon_id);
    exit;
}

function get_participation($hackathon_id, $member_id) {
    // Connexion à la base de données
    $connection = pg_connect("host=195.221.64.108 dbname=hackathon user=abdousalam password=abdousalam");
    $query = "SELECT * FROM public.participation WHERE hackathonid='$hackathon_id' AND memberid='$member_id'";
    $result = pg_query($connection, $query);
    $row = pg_fetch_assoc($result);
    if ($row !== false) {
        $participation = new Participation($row['id'], $row['hackathonid'], $row['memberid'], $row['roleid'], $row['equipeid']);
        return $participation;
    }
}

if (isset($user_id)) {
    $participation = new Participation(null, null, $user_id, null, null);
    $role_id = $participation->getRoleIdByUserId($user_id);
    if ($role_id == 4) {
        if (isset($_POST['request_submit'])) {
            $hackathon_id = $_POST['hackathon_id'];
            $titre = $_POST['titre'];
            $description = $_POST['description'];
            $assistance = new Assistance(null, $hackathon_id, $user_id, $titre, $description);
            $result = $assistance->createAssistance();
            if ($result) {
                echo '<div class="alert alert-success">Votre demande a été envoyée avec succès</div>';
            } else {
                echo '<div class="alert alert-danger">Erreur lors de l\'envoi de la demande. Veuillez réessayer plus tard.</div>';
            }
        }
        ?>
        <h2>Demande de renseignements :</h2>
        <form method="POST">
            <input type="hidden" name="hackathon_id" id="hackathon_id" value="<?php echo $hackathon->getId(); ?>">
            <div class="form-group">
                <label for="titre">Titre :</label>
                <input type="text" class="form-control" id="titre" name="titre" required>
            </div>
            <div class="form-group">
                <label for="description">Description :</label>
                <textarea class="form-control" id="description" name="description" rows="3" required></textarea>
            </div>
            <button type="submit" name="request_submit" class="btn btn-primary">Envoyer</button>
        </form>
        <?php
    } elseif ($role_id == 1) {
        if (isset($_POST['request_submit'])) {
            $hackathon_id = $_POST['hackathon_id'];
            $titre = $_POST['titre'];
            $description = $_POST['description'];
            $competence = new Competences(null, $hackathon_id, $user_id, $titre, $description);
            $result = $competence->createCompetences();
            if ($result) {
                echo '<div class="alert alert-success">Votre demande a été envoyée avec succès</div>';
            } else {
                echo '<div class="alert alert-danger">Erreur lors de l\'envoi de la demande. Veuillez réessayer plus tard.</div>';
            }
        }
        ?>
        <h2>Envoyer une compétence :</h2>
        <form method="POST">
            <input type="hidden" name="hackathon_id" id="hackathon_id" value="<?php echo $hackathon->getId(); ?>">
            <div class="form-group">
                <label for="titre">Titre :</label>
                <input type="text" class="form-control" id="titre" name="titre" required>
            </div>
            <div class="form-group">
                <label for="description">Description :</label>
                <textarea class="form-control" id="description" name="description" rows="3" required></textarea>
            </div>
            <button type="submit" name="request_submit" class="btn btn-primary">Ajouter</button>
        </form>
        <?php
    }
}

include $root . '/view/footer.html.php';
?>