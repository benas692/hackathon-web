<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
include "$root/view/header.html.php"; ?>

        <h1>Hackathon à venir :</h1>
        <?php
            foreach ($hackathonList as $h) {
                if ($h->getDateDebut() === null) {
                    print('<a href="/?object=hackathon&action=details&id=' . $h->getId().'">' . $h->getName().'</a><br>');
                }
            }
        ?>
        <h1>Hackathon en cours :</h1>
        <?php
            foreach ($hackathonList as $h) {
                if ($h->getDateDebut() !== null && $h->getDateFin() === null) {
                    print('<a href="/?object=hackathon&action=details&id=' . $h->getId().'">' . $h->getName().'</a><br>');
                }
            }
        ?>
        <h1>Hackathon passés :</h1>
        <?php
            foreach ($hackathonList as $h) {
                if ($h->getDateFin() != null) {
                    print('<a href="/?object=hackathon&action=details&id=' . $h->getId() . '">' . $h->getName() . '</a><br>');
                }
            }
        ?>


<?php include "$root/view/footer.html.php";
