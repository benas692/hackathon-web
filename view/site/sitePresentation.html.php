<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once './dal/bd.hackathon.inc.php';
require_once './model/Member.php';
include "$root/view/header.html.php";



function isLoggedIn() : bool {
    // Vérifie si une session est en cours
    if (session_status() === PHP_SESSION_ACTIVE) {
        // Vérifie si la variable de session "id" est définie et non nulle
        if (isset($_SESSION["id"]) && isset($_SESSION["email"])) {
            return true;
        }
    }
    return false;
}

if (isLoggedIn()) {
    // Afficher les éléments pour les utilisateurs connectés
    echo '<div id="accroche">Welcome to Hackat\'Web</div>';
    echo "<div id='personal-info'>
              <p>Informations personnelles :</p>
              <ul>
                  <li>Prénom : ".$_SESSION['firstname']."</li>
                  <li>Nom : ".$_SESSION['lastname']."</li>
                  <li>Téléphone : ".$_SESSION['telephone']."</li>
                  <li>Date de naissance : ".$_SESSION['date_naissance']."</li>
                  <li>Portfolio : ".$_SESSION['portfolio']."</li>
              </ul>
          </div>";
} else {
    // Afficher les éléments pour les utilisateurs non connectés
    echo '<div id="accroche">Welcome to Hackat\'Web</div>
          <br>
          <center>
            <h2>
                Hackat\'Web est une plateforme en ligne qui simplifie l\'organisation et la gestion des Hackathons. Grâce à ses fonctionnalités intuitives, vous pouvez facilement gérer les inscriptions, positionner les équipes, clôturer l\'événement et afficher les résultats en temps réel. Avec Hackat\'Web, vous pouvez planifier et exécuter un hackathon réussi, en réduisant les tracas administratifs et en vous concentrant sur la stimulation de la créativité et de l\'innovation. Que vous soyez un organisateur expérimenté ou novice, Hackat\'Web vous aidera à maximiser le potentiel de votre événement et à offrir une expérience inoubliable à vos participants.
            </h2>
          </center>';
}

echo "<style>
            #personal-info {
                border: 1px solid black;
                padding: 10px;
                margin-left: 50%;
                transform: translateX(-50%);
            }
      </style>";


include "$root/view/footer.html.php";
?>