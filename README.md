# hackathonweb

- Développée en PHP7

# Base de données

- Postgresql

- Paramètres de connexion à adapter dans le fichier env/db.ini

- Utilisation classe PHP PDO

- Scripts disponibles dans le dossier model/scripts

# Architecture de l'application

- Architecture en MVC

# Documentation

- Commentaires de code
- Typage PHP des fonctions
- Versioning