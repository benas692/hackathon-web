<?php
if ( $_SERVER["SCRIPT_FILENAME"] == __FILE__ ){
    $root="..";
}

require_once("$root/model/Hackathon.php");
require_once("$root/dal/bd.hackathon.inc.php");

// creation du menu burger
$burgerMenu = array();
$burgerMenu[] = Array("url"=>"./index.php?object=hackathon&action=all","label"=>"Tous");
//$burgerMenu[] = Array("url"=>"./index.php?objet=hackathon&action=showprojects","label"=>"Projects");
//$burgerMenu[] = Array("url"=>"./index.php?objet=hackathon&action=showResults","label"=>"Results");



// recuperation de l'action
if (isset($_GET["action"])){
    $action = $_GET["action"];
}
else {
    $action = "all";
}


// Gestion des différentes fonctionalités
switch($action) {

    case 'all':
        // Affichage de la liste des hackathons

        // 1 - Recuperation de la liste à partir de la BDD

        $hackathonList = getHackathons();

        // 2 - Affichage de la liste

        include "$root/view/hackathon/allHackathon.html.php";

        break;

    case 'details':

        // 1 - Recuperation des infos du hackathon

        $idHackathon = $_GET['id'];

        $hackathon = getHackathon($idHackathon);
        $inscrits = getInscriptions($idHackathon);

        // 2 - Affichage des infos du hackathon

        include "$root/view/hackathon/detailsHackathon.html.php";

        break;

    case 'registration':

        // 1 - Recuperation des infos du hackathon

        $hackathon = getHackathon($_GET['id']);

        if(!(isset($_GET['firstname']) and isset($_GET['lastname'])))
        {
            // 2a - Affichage du formulaire d'inscription

            include "$root/view/hackathon/registrationHackathon.html.php";
        }
        else
        {
            // 2b - Enregistrement des inscription

            insertInscription($hackathon->getId(),$_GET['firstname'],$_GET['lastname']);
            header('Location:/?object=hackathon&action=details&id='.$hackathon->getId());

        }
        break;

    case 'connexion':


        include "$root/view/hackathon/connexion.html.php";

        break;



    case 'inscription':


        include "$root/view/hackathon/inscription.html.php";

        break;


    case 'compte':
        include "$root/view/hackathon/compte.html.php";
        break;


    default:
        include "$root/view/error/400.html.php";

}