<?php
if ($_SERVER["SCRIPT_FILENAME"] == __FILE__) {
    $root = "..";
}

// creation du menu burger
$burgerMenu = array();
$burgerMenu[] = Array("url" => "./index.php?objet=site&action=presentation", "label" => "Presentation");
$burgerMenu[] = Array("url" => "./index.php?objet=site&action=inscription", "label" => "Inscription");
$burgerMenu[] = Array("url" => "./index.php?objet=site&action=connexion", "label" => "Connexion");

// start session
session_start();

// check if user is already logged in
if (isset($_SESSION["id"]) && isset($_SESSION["email"])) {
    $isLoggedIn = true;
} else {
    $isLoggedIn = false;
}

// retrieve action
if (isset($_GET["action"])) {
    $action = $_GET["action"];
} else {
    $action = "presentation";
}

// handle different functionalities
switch ($action) {
    case 'presentation':
        // call functions to get data needed for display
        // display the view
        include "$root/view/site/sitePresentation.html.php";
        break;

    case 'deconnexion':
        session_start();
        session_unset();
        session_destroy();
        header('Location: index.php?object=site&action=presentation');
        exit();
        break;

    case 'compte':
        include "$root/view/hackathon/compte.html.php";
        break;

    default:
        include "$root/view/error/400.html.php";
}
?>