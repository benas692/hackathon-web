drop table hackathon;
CREATE TABLE hackathon
(
    id          serial  NOT NULL,
    name        varchar NOT NULL UNIQUE,
    topic       text    NOT NULL,
    description text    NOT NULL,

    CONSTRAINT hackathon_pkey PRIMARY KEY (id)
);

drop table member;
CREATE TABLE member
(
    id        serial      NOT NULL,
    firstname varchar(20) NOT NULL,
    lastname  varchar(20) NULL,
    CONSTRAINT member_pkey PRIMARY KEY (id)

);

drop table role;
CREATE TABLE role
(
    id        serial      NOT NULL,
    name varchar(20) NOT NULL,
    CONSTRAINT role_pkey PRIMARY KEY (id)

);

drop table participation;
CREATE TABLE participation
(   id serial not null,
    hackathonid integer NOT NULL,
    memberid  integer NOT NULL,
    roleid integer not null,
    details text null,
    CONSTRAINT participation_pkey PRIMARY KEY (id)
);

ALTER TABLE participation
    ADD CONSTRAINT participation_fk_member FOREIGN KEY (memberid) REFERENCES member (id);
ALTER TABLE participation
    ADD CONSTRAINT participation_fk_hackathon FOREIGN KEY (hackathonid) REFERENCES hackathon (id);
ALTER TABLE participation
    ADD CONSTRAINT participation_fk_role FOREIGN KEY (roleid) REFERENCES role (id);
